# Author: Gabriel Santamaria <gabriel.santamaria@etu.utc.fr>
# SY02 - DLF
library('nloptr')
# Question 1

# Loading the CSV data file into the "data_1" variable
data_1 <- read.csv("data_1.csv")

# Checking that the data has been correctly loaded
(head(data_1))

# Question 2: height as a function of gdd

plot(
  x = data_1$gdd,
  y = data_1$height,
  xlab = "GDD",
  ylab = "Height"
)

# Question 3
segments(
  x0 = 590,
  y0 = 25,
  x1 = 1030,
  y1 = 25,
  col = "red",
  lwd = 3
)
segments(
  x0 = 590,
  y0 = 20,
  x1 = 590,
  y1 = 30,
  col = "red",
  lwd = 3
)
segments(
  x0 = 1030,
  y0 = 20,
  x1 = 1030,
  y1 = 30,
  col = "red",
  lwd = 3
)

text(
  x = 810,
  y = 26.5,
  labels = "Tallage",
  col = "red",
  cex = 1.2
)
segments(
  x0 = 1050,
  y0 = 25,
  x1 = 1800,
  y1 = 25,
  col = "blue",
  lwd = 3
)
segments(
  x0 = 1050,
  y0 = 20,
  x1 = 1050,
  y1 = 30,
  col = "blue",
  lwd = 3
)
segments(
  x0 = 1800,
  y0 = 20,
  x1 = 1800,
  y1 = 30,
  col = "blue",
  lwd = 3
)
text(
  x = 1425,
  y = 26.5,
  labels = "Montaison",
  col = "blue",
  cex = 1.2
)

plot(
  x = data_1$gdd,
  y = data_1$height,
  xlab = "GDD",
  ylab = "Height"
)
segments(
  x0 = 600,
  y0 = 16,
  x1 = 1050,
  y1 = 16,
  col = "red",
  lwd = 3
)
segments(
  x0 = 1050,
  y0 = 16,
  x1 = 1800,
  y1 = 76,
  col = "blue",
  lwd = 3
)

# Question 6

# Split the parts of the data in two (one for each phase)

tallage <- data_1[data_1$gdd <= 1100, ]
montaison <- data_1[data_1$gdd > 1100, ]

# Plot the two data
par(mfrow = c(1, 2))
plot(
  x = tallage$gdd,
  y = tallage$height,
  ylim = c(0, 90),
  main = "Tallage",
  xlab = "GDD",
  ylab = "Height"
)
plot(
  x = montaison$gdd,
  y = montaison$height,
  ylim = c(0, 90),
  main = "Montaison",
  xlab = "GDD",
  ylab = "Height"
)
par(mfrow = c(1, 1))

# Value of \mu:

mu <- mean(tallage$height)
(mu)


# Shift datas before fitting the lm model
nu <- montaison$height - mu
xi <- montaison$gdd - 1100

nu_hat <- lm(nu ~ xi)

# Display the "b" coefficient
(nu_hat[["coefficients"]])

# Part 1.5
# Question 1

# Positive part function: takes the positive part of the values of a given
# vector.
ppos <- function(v) {
  pmax(v, 0)
}

vect <- runif(n = 50, min = -10, max = 10)
vect <- ppos(vect)

# Question 2

generate <- function(gdd, mu, r, b, sigma_sq) {
  esp <- mu + b * ppos(gdd - r)
  rnorm(mean = esp,
        sd = sqrt(sigma_sq),
        n = length(gdd))
}

# Question 3
gheights <- generate(data_1$gdd, 17.13, 1100, 0.0815)
plot(data_1$gdd, gheights, xlab = "GDD", ylab = "Height (generated)")

summary (tallage$height)
var(tallage$height)


# Part 2


mask1 <- data_1$gdd < 1000
val_a <- mean(data_1$height[mask1])
val_b <- mean(data_1$height[mask1] ^ 2)

# Mu can be approached by the A value such that
mu_hat <- val_a
# same for sigma², it can be approached by B - (A)^2
sig_hat_s <- val_b - val_a ^ 2

mask2 <- (data_1$gdd >= 1200) & (data_1$gdd <= 1500)
val_c <- mean(data_1$height[mask2])
tilde_t_c <- mean(data_1$gdd[mask2])

mask3 <- (data_1$gdd > 1500)
val_d <- mean(data_1$height[mask3])
tilde_t_d <- mean(data_1$gdd[mask3])

b_hat <- (val_d - val_c) / (tilde_t_d - tilde_t_c)
r_hat <- tilde_t_c - (val_c - val_a) / b_hat

# Part 2.3

# Question 1

# Function that returns, based on a GDD and Height vectors
# an estimate of \mu, \sigma², b and r.
# We suppose that gdd and heights params have the same dim
estimate <- function(gdd, heights) {
  masks <- list(
    one = gdd < 1000,
    two = (gdd >= 1200) & (gdd <= 1500),
    three = gdd > 1500
  )
  A <- mean(heights[masks$one])
  B <- mean(heights[masks$one] ^ 2)
  C <- mean(heights[masks$two])
  D <- mean(heights[masks$three])
  
  T_C <- mean(gdd[masks$two])
  T_D <- mean(gdd[masks$three])
  
  b <- (D - C) / (T_D - T_C)
  r <- T_C - (C - A) / b
  
  list(
    mu = A,
    sigs = B - A ^ 2,
    b = b,
    r = r
  )
}

estimated <- estimate(data_1$gdd, data_1$height)

# Question 2

# For this question we'll use the same parameters used for the previous examople
r_trial <- estimate(data_1$gdd, generate(data_1$gdd, 15, 1100, 0.1, 4))$r

# Question 3

r_estimate <- function() {
  estimate(data_1$gdd, generate(data_1$gdd, 15, 1100, 0.1, 4))$r
}

simuls <- 10 ^ 8
r_v <- replicate(simuls, r_estimate())

# Question 4

hist(
  r_v,
  freq = FALSE,
  xlim = c(1050, 1150),
  xlab = "Value of r",
  ylab = "Proportion",
  main = "Distribution of r values",
  breaks = 1000
)
curve(
  dnorm(x, mean = 1100, sd = sqrt(110)),
  from = 1050,
  to = 1150,
  col = "blue",
  add = TRUE,
  lwd = 2
)

esp_r_v <- mean(r_v)

bias <- esp_r_v - r_trial


rmse <- (1 / simuls) * sum((r_trial - r_v) ^ 2)
(rmse)


rvar <- 1 / simuls * sum((r_v - esp_r_v) ^ 2)
(rvar)

rrmse <- sqrt(rmse)
(rrmse)

mask1 <- data$gdd < 1000
mask2 <- (data$gdd >= 1200) & (data$gdd <= 1500)
mask3 <- data$gdd > 1500
n_a <- sum(mask1)
n_c <- sum(mask2)
n_d <- sum(mask3)

bb <- 0.0811905
x <- (n_a * n_c * bb * (r_trial - tilde_t_c)) / (n_a + n_c)
p_c_a <- dnorm(x)
(p_c_a)

x2 <- (n_d * n_c * bb * (tilde_t_c - tilde_t_d)) / (n_d + n_c)
p_d_c <- dnorm(x)
(p_d_c)


# Part 4
# Question 2

estimated = estimate(data_1$gdd, generate(data_1$gdd, 15, 1100, 0.1, 4))

(paste("Value of r, ", estimated$r))

f <- function(b, r, sig) {
  sqrt(sig ^ 2 / b ^ 2 * (1 / n_a + (1 / (
    tilde_t_d - tilde_t_c
  ) ^ 2) * (
    (tilde_t_d - r) ^ 2 / n_c + (tilde_t_c - r) ^ 2 / n_d
  )))
}

conf_int <- function(alpha, e) {
  u <- qnorm(1 - alpha/2, mean = 0, sd = 1)
  var <- h(e$b, e$r, e$sigs)
  e$r + c(-1, 1) * u * var
}

(conf_int(0.05, estimated))

(conf_int(0.05, estimate(data_1$gdd, data_1$height)))

gen_ic <- function() {
  estimated = estimate(data_1$gdd, generate(data_1$gdd, 15, 1100, 0.1, 4))
  conf_int(0.05, estimated)
}

M = 10^5
ic_matrix <- replicate(M, gen_ic())

ic_lengths <- ic_matrix[2, ] - ic_matrix[1, ]
mean(ic_lengths)

coverage <- function(ic_matrix, val) {
  contains <- (ic_matrix[1, ] <= val) & (ic_matrix[2, ] >= val)
  coverage <- sum(contains) / length(contains)
  coverage
}

c_mean <- coverage(ic_matrix, 1100)
c_mean

rmin <- data_1$gdd[2]
rmax <- data_1$gdd[nrow(data_1)-1]
c(rmin, rmax)
profiled_likelihood <- function(r, gdd, height) {
  n <- length(gdd)
  bar_h <- mean(height)
  bar_pos_t_minus_r <- mean(ppos(gdd-r))
  tilde_b_r <- (
    sum((height-bar_h)*ppos(gdd-r))
    /
      sum((ppos(gdd-r)-bar_pos_t_minus_r)*ppos(gdd-r))
  )
  tilde_mu_r <- bar_h - tilde_b_r*bar_pos_t_minus_r
  tilde_sigma_sq_r <- mean((height-(tilde_mu_r+tilde_b_r*ppos(gdd-r)))**2)
  tilde_ell <- -n/2*(log(2*pi)+log(tilde_sigma_sq_r)+1)
  tilde_ell
}

library('nloptr')
maximum <- neldermead((rmax+rmin)/2, function(r) -profiled_likelihood(r, data_1$gdd, data_1$height), lower=rmin, upper=rmax)
rmax_value <- profiled_likelihood(maximum$par, data_1$gdd, data_1$height)
(rmax_value)
alpha <- 0.05
chi_squared_value <- qchisq(1-alpha, 1)

vpos <- rmax_value - (chi_squared_value / 2)

par(mfrow = c(1, 2))

curve(sapply(x, function(r) profiled_likelihood(r, data_1$gdd, data_1$height)), 
      from = rmin, to = rmax, ylab = "log-likelihood")
abline(vpos, 0, col = "red")
title(main = "Whole domain")

curve(sapply(x, function(r) profiled_likelihood(r, data_1$gdd, data_1$height)), 
      from = 1040, to = 1090, ylab = "log-likelihood")
abline(vpos, 0, col = "red")
abline(v = 1058, col = "blue")
abline(v = 1081, col = "blue")
title(main = "Zoomed (1040 to 1090)")

par(mfrow = c(1, 1))


bisection <- function(fun, a, b) {
  fa <- fun(a)
  fb <- fun(b)
  increasing <- (fa<0 && fb>=0)
  decreasing <- (fa>=0 && fb<0)
  if(!(increasing||decreasing)) stop('invalid a and b')
  c <- (a+b)/2
  while(a!=c && b!=c) {
    fc <- fun(c)
    if((fc>=0 && increasing)||(fc<0 && decreasing))
      b <- c
    else
      a <- c
    c <- (a+b)/2
  }
  c
}

ff <- function(r)  {
  profiled_likelihood(r, data_1$gdd, data_1$height) - vpos
}

binf <- bisection(ff, rmin, maximum$par)
bsup <- bisection(ff, maximum$par, rmax)
